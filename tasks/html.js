// paths
var paths = require('./paths')

// modules
var gulp = require('gulp'),
	beep = require('./beep'),
	plumber = require('gulp-plumber'),
	connect = require('gulp-connect'),
    decode = require('ent/decode'),
    prettify = require('gulp-prettify'),
	
	postcss = require('gulp-postcss'),
	postxml = require('gulp-postxml'),
	plugins = [
		require('postxml-import')({
			path: function (attr) {
                if (!/(\\|\/|\.)/.test(attr)) {
                    var module = attr;
                    if (/\d+$/.test(attr)) {
                        module = attr.replace(/\d+$/,'');
                    }
                    return 'modules/' + module + '/' + attr + '.htm';
                }
				return attr;
			}
		}),
		require('postxml-import')(),
		require('postxml-beml')(),
		require('postxml-imgalt')(),
		require('postxml-placeholder')({
			url: 'http://placehold.alanev.ru/'
		}),
		require('postxml-image-size')({
			cwd: paths.dest
		}),
		require('postxml-wrap')(),
		require('postxml-repeat')(),
        (function () {
            return function ($) {
                $('html').html(decode($('html').html()));
            }
        })()
	]
	;;

// task
var task = function () {
	return gulp.src(paths.html.src)
		.pipe(plumber(beep))
		.pipe(postxml(plugins))
		.pipe(prettify())
		.pipe(plumber.stop())
		.pipe(gulp.dest(paths.html.dest))
		.pipe(connect.reload())
		;
}

// module
module.exports = task;